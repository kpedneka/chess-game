# Text-based Chess
A console based chess game built using Java 8.

### Board
The board is displayed on every turn
![board](https://bytebucket.org/kpedneka/chess-game/raw/bb69eebb5a271bdaaa76bab8da95af9c1cd6aae5/Chess/docs/basic%20board.png)

### Basic moves
Basic moves are performed through user input in the following format
![moves](https://bytebucket.org/kpedneka/chess-game/raw/bb69eebb5a271bdaaa76bab8da95af9c1cd6aae5/Chess/docs/moves.png)

### Check
The game can detect checks and prompt the user that they are under check. While under check, this user cannot make any move that does not reset the check flag.
![check](https://bytebucket.org/kpedneka/chess-game/raw/bb69eebb5a271bdaaa76bab8da95af9c1cd6aae5/Chess/docs/check.png)

### Checkmate
If no moves would result in resetting the check flag, the game can detect that a checkmate has been reached.
![checkmate](https://bytebucket.org/kpedneka/chess-game/raw/bb69eebb5a271bdaaa76bab8da95af9c1cd6aae5/Chess/docs/checkmate.png)