package piece;

import driver.Chess;

/**
 * @author Rachana Thanawala(rt468)
 * The Rook can move only horizontally or vertically
 * Either Rank or File must be constant
 * Location of piece is updated if moved.
 * in Between pieces are checked in class Chess
 */

public class Rook extends Piece {
	public Rook(String color, String location) {
		this.color = color;
		this.location = location;
		this.name = color.equals("black") ? "bR" : "wR";
		this.hasMoved = false;
	}
	
	public void undo() {
		this.location = this.lastLocation;
	}
	public boolean canMove(String newLocation) {
		if (this.isValid(newLocation)) {
			return true;

		}
		//System.out.println("Illegal move, try again");
		return false;
	}

	
	public boolean move(String newLocation) { 
		//checking for castling
		if(!hasMoved&&Chess.incastling)
		{
			this.hasMoved=true;
			this.lastLocation = this.location;
			this.location = newLocation;
			return true;
		}
		else if (this.isValid(newLocation)) {
			this.hasMoved=true;
			this.lastLocation = this.location;
			this.location = newLocation;
			return true;
		}
		return false;
	}
	public boolean isValid(String newLocation) { 
		int newRank = Piece.getRank(newLocation);
		int newFile = Piece.getFile(newLocation);
		int prevFile = Piece.getFile(this.location);
		int prevRank = Piece.getRank(this.location);
		int rankChange = newRank-prevRank;
		int fileChange = newFile-prevFile;
		if(newFile ==prevFile && newRank!= prevRank && Math.abs(rankChange)<8) 
		{
			return true;
		}
		else if(newRank == prevRank && newFile!= prevFile && Math.abs(fileChange)<8)
		{
			return true;
		}
		else
			return false;
	}				
}

