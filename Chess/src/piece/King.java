package piece;

/**
 * The movement of the king is checked. 
 * The king can move only one location away, side ways, up/down and diagonal
 * rankChange and fileChange are checked to validate movement
 * Location of piece is then updated
 * @author Rachana Thanawala(rt468)
 *
 */
public class King extends Piece {
	public King(String color, String location) {
		this.color = color;
		this.location = location;
		this.name = color.equals("black") ? "bK" : "wK";
		this.hasMoved = false;
	}
	public boolean canMove(String newLocation) {
		if (this.isValid(newLocation)) {
			return true;

		}
		//System.out.println("Ilegal move, try again");
		return false;
	}

	
	public void undo() {
		this.location = this.lastLocation;
	}
		
	public boolean move(String newLocation) { 
		//white king to the left - castling
		if(this.hasMoved==false&&this.location.equals("e1")&&newLocation.equals("g1"))
		{
			this.hasMoved=true;
			this.lastLocation = this.location;
			this.location = newLocation;
			//System.out.println("Piece has moved");
			return true;
		}
		//white king to the right - castling
		else if(this.hasMoved==false&&this.location.equals("e1")&&newLocation.equals("b1"))
		{
			this.hasMoved=true;
			this.lastLocation = this.location;
			this.location = newLocation;
			//System.out.println("Piece has moved");
			return true;
		}
		//black king to the left - castling
		else if(this.hasMoved==false&&this.location.equals("e8")&&newLocation.equals("g8"))
		{
			this.hasMoved=true;
			this.lastLocation = this.location;
			this.location = newLocation;
			//System.out.println("Piece has moved");
			return true;
		}
		//white king to the right - castling
		else if(this.hasMoved==false&&this.location.equals("e8")&&newLocation.equals("b8"))
		{
			this.hasMoved=true;
			this.lastLocation = this.location;
			this.location = newLocation;
			//System.out.println("Piece has moved");
			return true;
		}
						
		else if (this.isValid(newLocation)) {
			this.hasMoved=true;
			this.lastLocation = this.location;
			this.location = newLocation;
			return true;
		}
		//System.out.println("Illegal move, try again");
		return false;
	}
	public boolean isValid(String newLocation) { 
		int newRank = Piece.getRank(newLocation);
		int newFile = Piece.getFile(newLocation);
		int prevFile = Piece.getFile(this.location);
		int prevRank = Piece.getRank(this.location);
			int rankChange = newRank-prevRank;
			int fileChange = newFile-prevFile;
		//	System.out.println(newLocation+" "+this.color+" "+rankChange+fileChange);
		
			//up or down
			if(newFile ==prevFile && Math.abs(rankChange)==1) 
			{
				return true;
			}
			
			//right or left
			else if(newRank == prevRank && Math.abs(fileChange)==1)
			{
				return true;
			}
			//diagonal
			else if(Math.abs(rankChange)==1 && Math.abs(fileChange)==1)
			{
				return true;
			}
			else
				return false;
	
	}
}