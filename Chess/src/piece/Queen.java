package piece;
/**
 * @author Rachana Thanawala(rt468)
 * The Queen can make any moves
 * changeRank and changeFile must be less than 7. 
 * Location of piece is updated if moved.
 * in Between pieces are chekcd in class Chess
 */

public class Queen extends Piece {
	public Queen(String color, String location) {
		this.color = color;
		this.location = location;
		this.name = color.equals("black") ? "bQ" : "wQ";
	}
	
	public void undo() {
		this.location = this.lastLocation;
	}
	public boolean canMove(String newLocation) {
		if (this.isValid(newLocation)) {
			return true;

		}
		//System.out.println("Ilegal move, try again");
		return false;
	}

	
	public boolean move(String newLocation) { 
		if (this.isValid(newLocation)) {
			this.hasMoved=true;
			this.lastLocation = this.location;
			this.location = newLocation;
			return true;
		}
		//System.out.println("Ilegal move, try again");
		return false;
	}
	public boolean isValid(String newLocation) { 
		int newRank = Piece.getRank(newLocation);
		int newFile = Piece.getFile(newLocation);
		int prevFile = Piece.getFile(this.location);
		int prevRank = Piece.getRank(this.location);
		int rankChange = newRank-prevRank;
		int fileChange = newFile-prevFile;
		//System.out.println(newLocation+" "+this.color+" "+rankChange+fileChange);
		if(Math.abs(rankChange) < 8 && Math.abs(fileChange) < 8) 
			{
				return true;
			}
		else
			return false;	
	}
}