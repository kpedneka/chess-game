package piece;

import driver.Chess;

/**
 * Pawn class for the chess game. A pawn can only move in one
 * direction in one file. The only time it can change files is
 * when it can move diagonally to capture another piece.
 * A pawn can also transform if it reaches the other side the board
 * implementation of validity and capture in class Chess
 * Location is updated
 * @author Kunal Pednekar(ksp101), Rachana Thanawala(rt468)
 *
 */
public class Pawn extends Piece {
	public Pawn(String color, String location) {
		this.color = color;
		this.location = location;
		this.name = color.equals("black") ? "bp" : "wp";
		this.hasMoved = false;
	}
	
	public boolean canMove(String newLocation) {
		if (this.isValid(newLocation)) {
			return true;

		}
		//System.out.println("Ilegal move, try again");
		return false;
	}

	public void undo() {
		this.location = this.lastLocation;
	}
		
	public boolean move(String newLocation) {
		if (this.isValid(newLocation)) {
			this.hasMoved=true;
			this.lastLocation = this.location;	
			this.location = newLocation;
			return true;	
		}
		return false;
	}

	public boolean isValid(String newLocation) { 
		int newRank = Piece.getRank(newLocation);
		int newFile = Piece.getFile(newLocation);
		int prevFile = Piece.getFile(this.location);
		int prevRank = Piece.getRank(this.location);
		int rankChange = newRank-prevRank;
		int fileChange = newFile-prevFile;
		if(Chess.pawncaptured==true)
			return true;
		else if(prevFile==newFile)
		{
			if (!this.color.equals("white") && rankChange == -1) 
			{
				return true;
			}
			else if (this.color.equals("white") && rankChange == 1) 
			{
				return true;
			}
			else if (!this.color.equals("white") && rankChange == -2 &&!hasMoved)
			{
				return true;
			}
			else if (this.color.equals("white") && rankChange == 2 && !hasMoved)
			{
				return true;
			}
			else 
				return false;
		}
		return false;
	}
}
