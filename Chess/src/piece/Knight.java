package piece;
/**
 * @author Rachana Thanawala(rt468)
 * The Knight can jump over pieces
 * changeRank must be 2 and changeFile must be 1 or vice versa. 
 * Location of piece is updated if moved.
 * in Between pieces are checked in class Chess
 */


public class Knight extends Piece {
	public Knight(String color, String location) {
		this.color = color;
		this.location = location;
		this.name = color.equals("black") ? "bN" : "wN";
	}
	
	public void undo() {
		this.location = this.lastLocation;
	}
	public boolean canMove(String newLocation) {
		if (this.isValid(newLocation)) {
			return true;
		}
		//System.out.println("Ilegal move, try again");
		return false;
	}

	
		
	public boolean move(String newLocation) { 
		if (this.isValid(newLocation)) {
			this.hasMoved=true;
			this.lastLocation = this.location;
			this.location = newLocation;
			return true;
		}
		//System.out.println("Ilegal move, try again KK");
		return false;
	}
		
	public boolean isValid(String newLocation) 
	{
	int newRank = Piece.getRank(newLocation);
	int newFile = Piece.getFile(newLocation);
	int prevFile = Piece.getFile(this.location);
	int prevRank = Piece.getRank(this.location);
		int rankChange = newRank-prevRank;
		int fileChange = newFile-prevFile;
		//System.out.println(newLocation+" "+this.color+" "+rankChange+fileChange);
		
		if (Math.abs(fileChange) == 1 && Math.abs(rankChange) == 2) 
		{
			return true;
		}
		else if (Math.abs(fileChange) == 2 && Math.abs(rankChange) == 1)
		{
			return true;
		}
		else
			return false;
	}
}