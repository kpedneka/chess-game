package piece;

/**
 * 
 * Abstract class Piece acts as a blueprint for all the chess pieces in the game.
 * @author Kunal Pednekar(ksp101)
 *
 */
public abstract class Piece {
	/** 
	 * {@link color} - color of the piece
	 * {@link location} - location of the piece
	 * {@link name} - name of the piece
	 */
	public String color, location, lastLocation, name;
	protected boolean hasMoved;
	
	/**
	 * Returns location in a format that is appropriate to update the board
	 * @param location - String in format that represents position on a chess board (eg.: e4)
	 * @return Integer value representing row value in board
	 */
	public static int getRank(String location) {
		return Character.getNumericValue(location.charAt(1));
	}
	
	/**
	 * Returns location in a format that is appropriate to update the board
	 * @param location - String in format that represents position on a chess board (eg.: e4)
	 * @return Integer value representing column value in board
	 */
	public static int getFile(String location) {
		return (int)(location.charAt(0) - 'a');
	}
	
	/**
	 * Encodes row, column values into a String format that represents position on a chess board
	 * @param rank - row value
	 * @param file - column value
	 * @return - String representing position on the board, empty string otherwise
	 */
	public static String encodeLocation(int rank, int file) {
		StringBuilder sb = new StringBuilder();
		if ((rank >= 1 && rank <=8) && (file >= 0 && file < 8)) {
			sb.append((char)(file+'a'));
			sb.append(rank);
			return sb.toString();	
		}
		return "";
	}
	public abstract void undo();
	public abstract boolean move(String newLocation);
	public abstract boolean canMove(String newLocation);
	public abstract boolean isValid(String newLocation);

}
