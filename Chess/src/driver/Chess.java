package driver;

import java.util.ArrayList;
import java.util.Scanner;

import board.Board;
import piece.Piece;
import player.Player;

/**
 * The Chess class is responsible for creating an instance of a chess game.
 * This class contains the main function to run the chess game application.
 * Each instance has a {@code Board board}, {@code Player white}, {@code Player black}.
 * It also maintains state about the progress of a game. 
 * @author Kunal Pednekar(ksp101), Rachana Thanawala(rt468)
 *
 */
public class Chess {
	Board board;
	Player white, black;
	ArrayList<String> checks;
	String lastMove;
	Piece lastCaptured;

	/**
	 * {@link whiteMove} - boolean keeping track of which player's turn it is
	 * {@link drawOffer} - boolean keeping track of "draw?" input from player
	 * {@link finished} - boolean keeping track of resign or accepting draw
	 * {@link promotion} - boolean keeping track of pawn promotion on the current turn
	 * {@link whiteUnderCheck, blackUnderCheck} - boolean keeping track of which player is under check
	 * {@link checkmate} - boolean keeping track of checkmate status of game
	 * {@link onlyMove} - boolean keeping track if only 1 legal move is left on check
	 * {@link castle} - boolean, performing the castling move
	 */
	private boolean whiteMove, drawOffer, finished, promotion,castled, enpassant, whiteUnderCheck, blackUnderCheck, checkmate, onlyMove;
	public static boolean incastling, pawncaptured;
	private String enpassant_color;
	/**
	 * Constructor for the Chess game. A call to this constructor creates a new game of Chess.
	 */
	public Chess() {
		this.board = new Board();
		this.black = new Player("black");
		this.white = new Player("white");
		this.board.updateBoard(this.black, this.white);
		this.whiteMove = true;
		this.promotion = false;
		this.whiteUnderCheck = false;
		this.blackUnderCheck = false;
		this.checks = new ArrayList<String>();
		this.lastMove = "";
		this.lastCaptured = null;
		this.castled = false;
		incastling = false;
		pawncaptured = false;
		this.enpassant = false;
		this.enpassant_color="";
	}


	/**
	 * Validates if input is a type of piece
	 * @param piece - the input to be tested
	 * @return - true if p matches a piece type, false otherwise
	 */
	private static boolean isPiece(String piece) {
		return piece.equals("N") || piece.equals("Q") || piece.equals("R") || piece.equals("B");
	}

	/**
	 * The printBoard function call forwards the call to the Board class in order to display
	 * the current state of the board. This function is automatically called at the end of each turn.
	 */
	public void printBoard() {
		this.board.printBoard();
	}

	/**
	 * Pawn promotion function. Invokes call to move regardless of successful promotion.
	 * If promotion succeeds, piece type changes, otherwise piece stays the same.
	 * @param location - the current location of the piece
	 * @param newLocation - the proposed move location for the piece
	 * @param piece - the desired piece promotion
	 */
	private void promote(String location, String newLocation, String piece) {
		//System.out.println("In promotion function");
		if (this.move(location, newLocation)) {
			//System.out.println("calling promotion function in player class");
			if (this.whiteMove) this.white.promote(newLocation, piece);
			else this.black.promote(newLocation, piece);
			// change game state
			this.promotion = true;
			this.board.updateBoard(this.white, this.black);
			this.underCheck();
			this.whiteMove = !this.whiteMove;
		}
	}

	/**
	 * To check for capture, the location diagonal on either side of the current location is checked. 
	 * It doesnt allow one to capture its own pawn
	 * If it is not empty and occupied by a piece of the opponent the pawn is allowed to move diagonal else no 
	 * @param curr - initial location
	 * @param next - destination location
	 * @return boolean value
	 */
	private boolean PawnCapture(String curr, String next) {
		pawncaptured = false;

		int prevrank = Piece.getRank(curr), prevfile = Piece.getFile(curr);

		int newRank = Piece.getRank(next), newFile = Piece.getFile(next);

		int rankChange = newRank-prevrank, fileChange = newFile -prevfile;
		//System.out.println(enpassant);
		//enpassant
		if(enpassant && this.board.getPiece(Piece.encodeLocation(newRank,newFile)).equals("")&&Math.abs(rankChange)==1&&Math.abs(fileChange)==1)
		{
			//System.out.println("enpassant");
			//capturing pawn on the right. current piece = white	
			if((rankChange>0&&fileChange>0)&&enpassant_color.equals("black")&&this.whiteMove&&this.board.getPiece(Piece.encodeLocation(prevrank,prevfile)).charAt(0)=='w'&& Math.abs(newFile-prevfile)==1 && (newRank-prevrank==1)&& this.board.getPiece(Piece.encodeLocation(prevrank,prevfile+1)).charAt(0)=='b')
			{	
				//pawncaptured=true;
				//System.out.println("Captured");
				this.black.kill(Piece.encodeLocation(prevrank, prevfile+1));
				return true;
			}
			//capturing pawn on the left. current piece = white
			else if((rankChange>0&&fileChange<0)&&enpassant_color.equals("black")&&this.whiteMove&&this.board.getPiece(Piece.encodeLocation(prevrank,prevfile)).charAt(0)=='w'&& Math.abs(newFile-prevfile)==1 && (newRank-prevrank==1)&& this.board.getPiece(Piece.encodeLocation(prevrank,prevfile-1)).charAt(0)=='b')
			{	
				//pawncaptured=true;
				//System.out.println("Captured");
				this.black.kill(Piece.encodeLocation(prevrank, prevfile-1));
				return true;
			}
			//capturing pawn on the right. current piece = black
			else if(rankChange<0&&fileChange>0&&enpassant_color.equals("white")&&this.board.getPiece(Piece.encodeLocation(prevrank,prevfile)).charAt(0)=='b'&& Math.abs(newFile-prevfile)==1 && (newRank-prevrank==-1)&& this.board.getPiece(Piece.encodeLocation(prevrank, prevfile+1)).charAt(0)=='w')
			{
				//pawncaptured = true;
				//System.out.println("Captured");
				this.white.kill(Piece.encodeLocation(prevrank, prevfile+1));
				return true;
			}
			//capturing pawn on the left. current piece = white
			else if(rankChange<0&&fileChange<0&&enpassant_color.equals("white")&&this.board.getPiece(Piece.encodeLocation(prevrank,prevfile)).charAt(0)=='b'&& Math.abs(newFile-prevfile)==1 && (newRank-prevrank==-1)&& this.board.getPiece(Piece.encodeLocation(prevrank, prevfile-1)).charAt(0)=='w')
			{
				//pawncaptured = true;
				//System.out.println("Captured");
				this.white.kill(Piece.encodeLocation(prevrank, prevfile-1));
				return true;
			}
			else 
				return false;
		}
		//capturing by pawn
		//check if the location diagonal to the current location is not empty
		else if(!this.board.getPiece(next).equals("")&&Math.abs(rankChange)==1&&Math.abs(fileChange)==1)
		{
			//check if the new location (forward diagonal) is occupied by the opponent
			//current piece = white
			if(this.whiteMove&&this.board.getPiece(curr).charAt(0)=='w'&& this.board.getPiece(next).charAt(0)=='b')
			{	
				this.black.kill(next);
				return true;
			}
			//current piece = black
			else if(this.board.getPiece(curr).charAt(0)=='b'&& this.board.getPiece(next).charAt(0)=='w')
			{
				//System.out.println("Captured");
				this.white.kill(next);
				return true;
			}
			else 
				return false;
		}
		else {
			return false;
		}
	}
	
	/**
	 * This function tests the movements of a pawn.
	 * The location change includes only a change in rank except during enpassment and capture
	 * To check for capture, the location diagonal on either side of the current location is checked. 
	 * If it is not empty and occupied by a piece of the opponent the pawn is allowed to move diagonal else no 
	 * @param curr - initial location
	 * @param next - destination location
	 * @return boolean value
	 */
	private boolean testValidatePawnPath(String curr, String next) {
		int prevrank = Piece.getRank(curr), prevfile = Piece.getFile(curr);

		int newRank = Piece.getRank(next), newFile = Piece.getFile(next);

		int rankChange = newRank-prevrank, fileChange = newFile -prevfile;

		//check for move validity. not capture or enpassent
		if(prevfile==newFile&&this.board.getPiece(next).equals(""))
		{
			if (!this.whiteMove && rankChange == -1) 
				return true;
			else if (this.whiteMove && rankChange == 1) 
				return true;
			//need to check hasMoved
			else if (!this.whiteMove && rankChange == -2)
				return true;
			//need to check hasMoved
			else if (this.whiteMove && rankChange == 2)
				return true;
			else 
			return false;
		}
		//capturing by pawn
		//check if the location diagonal to the current location is not empty
		else if(!this.board.getPiece(next).equals("")&&Math.abs(rankChange)==1&&Math.abs(fileChange)==1)
		{
			//check if the new location (forward diagonal) is occupied by the opponent
			//current piece = white
			if(this.board.getPiece(curr).charAt(0)=='w'&& Math.abs(newFile-prevfile)==1 && (newRank-prevrank==1)&& this.board.getPiece(next).charAt(0)=='b')
				return true;
			//current piece = black
			else if(this.board.getPiece(curr).charAt(0)=='b'&& Math.abs(newFile-prevfile)==1 && (newRank-prevrank==-1)&& this.board.getPiece(next).charAt(0)=='w')
				return true;
			else 
				return false;
		}
		else 
		return false;

	}
	
	/**
	 * Test validation function to simulate if a move is either
	 * within same file, same rank, or diagonal. The function
	 * also checks if there is an obstacle in the path. Does
	 * not perform capture if last position is opponent piece
	 * Exception is only made for Knight.
	 * @param curr - is the current (start) position
	 * @param next - is the final (end) position
	 * @return - true if path is valid and no obstacles. False otherwise.
	 */
	private boolean testValidatePath(String curr, String next) {
		
		int rank = Piece.getRank(curr), file = Piece.getFile(curr);
		int newRank = Piece.getRank(next), newFile = Piece.getFile(next);

		// diagonal
		if (Math.abs(rank-newRank) == Math.abs(file-newFile)) {
			int steps = Math.abs(rank-newRank);
			int step = rank < newRank ? 1 : -1;
			int fstep = file < newFile ? 1 : -1;
			for (int i = 0; i < steps-1; i++) {
				if (!this.board.getPiece(Piece.encodeLocation(rank+step, file+fstep)).equals(""))
					return false;
				//update rank and file
				rank += step;
				file += step;
			}
			if(this.board.getPiece(next).equals(""))
				return true;
			//check if the final location is occupied by the opponent, if yes capture, if empty allow movement, else invalid move
			//also check if its not a pawn since the capture move is different for a pawn
			//current piece = white piece
			
			return true;
		}
		// up/down
		else if (file-newFile == 0 && rank-newRank != 0) {
			int steps = Math.abs(rank-newRank);
			int step = rank-newRank < 0 ? 1 : -1;
			for (int i = 0; i < steps-1; i++) {
				if (!this.board.getPiece(Piece.encodeLocation(rank+step, file)).equals(""))
					return false;
				// update rank
				rank += step;
			}
			if(this.board.getPiece(next).equals(""))
				return true;
			//same as previous
			return true;
		}
		// side to side
		else if (file-newFile != 0 && rank-newRank == 0) {
			int steps = Math.abs(file-newFile);
			int step = file-newFile < 0 ? 1 : -1;
			for (int i = rank; i < steps-1; i++) {
				if (!this.board.getPiece(Piece.encodeLocation(rank, file+step)).equals(""))
					return false;
				// update file
				file += step;
			}
			if(this.board.getPiece(next).equals(""))
				return true;
			//same as previous
			return true;
		}
		//all other moves are invalid
		return false;
	}

	/**
	 * Validation function to enforce that a move is either
	 * within same file, same rank, or diagonal. The function
	 * also checks if there is an obstacle in the path. Does
	 * not check last position because that is a capture. 
	 * Exception is only made for Knight.
	 * @param curr - is the current (start) position
	 * @param next - is the final (end) position
	 * @return - true if path is valid and no obstacles. False otherwise.
	 */
	private boolean validatePath(String curr, String next) {
		int rank = Piece.getRank(curr), file = Piece.getFile(curr);
		int newRank = Piece.getRank(next), newFile = Piece.getFile(next);
		// diagonal
		if (Math.abs(rank-newRank) == Math.abs(file-newFile)) {
			//System.out.println("Diagonal");
			int steps = Math.abs(rank-newRank);
			int step = rank-newRank < 0 ? 1 : -1;
			int fstep = file-newFile < 0 ? 1 : -1;
			for (int i = 0; i < steps-1; i++) {
				if (!this.board.getPiece(Piece.encodeLocation(rank+step, file+fstep)).equals(""))
					return false;
				//update rank and file
				rank += step;
				file += fstep;
			}
			if(this.board.getPiece(Piece.encodeLocation(newRank, newFile)).equals(""))
				return true;
			//check if the final location is occupied by the opponent, if yes capture, if empty allow movement, else invalid move
			//also check if its not a pawn since the capture move is different for a pawn
			//current piece = white piece
			else if (rank+step == newRank && file+fstep ==newFile &&(this.board.getPiece(curr)).charAt(0)=='w' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='b'&&(this.board.getPiece(curr)).charAt(1)!='p')
			{
				//System.out.println("captured");
				this.black.kill(next);
				return true;
			}
			//current piece = black piece
			else if (rank+step == newRank && file+fstep ==newFile && (this.board.getPiece(curr)).charAt(0)=='b' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='w'&&(this.board.getPiece(curr)).charAt(1)!='p')
			{
				//System.out.println("captured");
				//System.out.println(this.board.getPiece(Piece.encodeLocation(newRank, newFile)));
				this.white.kill(next);
				return true;
			}
			//current piece and final location = white piece
			else if (rank+step == newRank && file+fstep ==newFile && (this.board.getPiece(curr)).charAt(0)=='w' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='w'&&(this.board.getPiece(curr)).charAt(1)!='p') {
				//System.out.println("no capture 3");
				return false;
			}
				
			//current piece and final location = white piece
			else if (rank+step == newRank && file+fstep ==newFile && (this.board.getPiece(curr)).charAt(0)=='b' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='b'&&(this.board.getPiece(curr)).charAt(1)!='p') {
				//System.out.println("no capture 4");
				return false;
			}
			else
				return true;
		}
		// up/down
		else if (file-newFile == 0 && rank-newRank != 0) {
			//System.out.println("Up and down");
			int steps = Math.abs(rank-newRank);
			int step = rank-newRank < 0 ? 1 : -1;
			for (int i = 0; i < steps-1; i++) {
				//System.out.println(8-rank+" "+steps+" "+Piece.encodeLocation(rank, file));

				if (!this.board.getPiece(Piece.encodeLocation(rank+step, file)).equals(""))
					return false;
				// update rank
				rank += step;
			}
			if(this.board.getPiece(Piece.encodeLocation(newRank, newFile)).equals(""))
				return true;
			//same as previous
			else if (rank+step == newRank && file ==newFile &&(this.board.getPiece(curr)).charAt(0)=='w' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='b'&&(this.board.getPiece(curr)).charAt(1)!='p')
			{
				//System.out.println("captured");
				this.black.kill(next);
				return true;
			}
			else if (rank+step == newRank && file ==newFile && (this.board.getPiece(curr)).charAt(0)=='b' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='w'&&(this.board.getPiece(curr)).charAt(1)!='p')
			{
				//System.out.println("captured");
				this.white.kill(next);
				return true;
			}
			else if (rank+step == newRank && file ==newFile && (this.board.getPiece(curr)).charAt(0)=='w' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='w'&&(this.board.getPiece(curr)).charAt(1)!='p')
				return false;
			else if (rank+step == newRank && file ==newFile && (this.board.getPiece(curr)).charAt(0)=='b' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='b'&&(this.board.getPiece(curr)).charAt(1)!='p')
				return false;
			else
				return true;
		}
		// side to side
		else if (file-newFile != 0 && rank-newRank == 0) {
			//System.out.println("Side to side");
			int steps = Math.abs(file-newFile);
			int step = file-newFile < 0 ? 1 : -1;
			for (int i = 0; i < steps-1; i++) {
				if (!this.board.getPiece(Piece.encodeLocation(rank, file+step)).equals(""))
					return false;
				// update file
				file += step;
			}
			if(this.board.getPiece(Piece.encodeLocation(newRank, newFile)).equals(""))
				return true;
			//same as previous
			else if (rank == newRank && file+step ==newFile &&(this.board.getPiece(curr)).charAt(0)=='w' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='b'&&(this.board.getPiece(curr)).charAt(1)!='p')
			{
				//System.out.println("captured");
				this.black.kill(next);
				return true;
			}
			else if (rank == newRank && file+step ==newFile && (this.board.getPiece(curr)).charAt(0)=='b' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='w'&&(this.board.getPiece(curr)).charAt(1)!='p')
			{
				//System.out.println("captured");
				this.white.kill(next);
				return true;
			}
			else if (rank== newRank && file+step ==newFile && (this.board.getPiece(curr)).charAt(0)=='w' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='w'&&(this.board.getPiece(curr)).charAt(1)!='p')
				return false;
			else if (rank== newRank && file+step ==newFile && (this.board.getPiece(curr)).charAt(0)=='b' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='b'&&(this.board.getPiece(curr)).charAt(1)!='p')
				return false;
			else
				return true;
		}
		
		//knight has different moves. side + up and can jump
		else if((Math.abs(file-newFile)==2&&Math.abs(rank-newRank)==1)||(Math.abs(file-newFile)==1&&Math.abs(rank-newRank)==2))
		{
			//System.out.println("In knight");
			if(this.board.getPiece(Piece.encodeLocation(newRank, newFile)).equals(""))
				return true;
			//same as previous
			else if ((this.board.getPiece(curr)).charAt(0)=='w' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='b'&&(this.board.getPiece(curr)).charAt(1)!='p')
			{
				//System.out.println("captured");
				this.black.kill(next);
				return true;
			}
			else if ((this.board.getPiece(curr)).charAt(0)=='b' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='w'&&(this.board.getPiece(curr)).charAt(1)!='p')
			{
				//System.out.println("captured");
				this.white.kill(next);
				return true;
			}
			else if ((this.board.getPiece(curr)).charAt(0)=='w' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='w'&&(this.board.getPiece(curr)).charAt(1)!='p')
				return false;
			else if ((this.board.getPiece(curr)).charAt(0)=='b' && (this.board.getPiece(Piece.encodeLocation(newRank, newFile))).charAt(0)=='b'&&(this.board.getPiece(curr)).charAt(1)!='p')
				return false;
			else
				return true;
		}
		
		//all other moves are invalid
		else
		return false;

	}
	
	/**
	 * Function to test detect a check during a game. Called from main() after every turn.
	 * Has access to both players and their pieces. Responsible for resetting check flag
	 * on every invocation.
	 */
	private boolean testUnderCheck(String location) {
		Player player = this.whiteMove ? this.white : this.black;
		ArrayList<String> moves = new ArrayList<String>();
		// System.out.println("in check again for "+otherPlayer.color);
		// for all pieces	
		for (Piece p : player.pieces) {
			// check if the piece can move to the king's location
			if (this.canMove(p.location, location)) {
				//System.out.println("Setting check flag "+player.color+"\n");
				// set appropriate flag for check
				if (player.color.equals("white")) this.blackUnderCheck = true;
				else this.whiteUnderCheck = true;
				// add check to arraylist
				//System.out.println(p.location+" can move to "+otherPlayer.getKingLocation());
				moves.add(p.location+" "+location);
			}
		}
		
		if (moves.size() > 0) {
			//testCheckmate();
			return true;
		}
		// reset check flag
		return false;
	}

	/**
	 * Function to detect a check during a game. Called from main() after every turn.
	 * Has access to both players and their pieces. Responsible for resetting check flag
	 * on every invocation.
	 */
	private boolean underCheck() {
		Player player = this.whiteMove ? this.white : this.black;
		Player otherPlayer = this.whiteMove ? this.black : this.white;
		ArrayList<String> moves = new ArrayList<String>();
		// System.out.println("in check again for "+otherPlayer.color);
		// for all pieces	
		for (Piece p : player.pieces) {
			// check if the piece can move to the king's location
			if (this.canMove(p.location, otherPlayer.getKingLocation())) {
				//System.out.println("Setting check flag "+player.color+"\n");
				// set appropriate flag for check
				if (player.color.equals("white")) this.blackUnderCheck = true;
				else this.whiteUnderCheck = true;
				// add check to arraylist
				//System.out.println(p.location+" can move to "+otherPlayer.getKingLocation());
				moves.add(p.location+" "+otherPlayer.getKingLocation());
			}
		}
		this.checks = moves;
		
		if (this.checks.size() > 0) {
			testCheckmate();
			return true;
		}
		// System.out.println("resetting flag ");
		// System.out.println((otherPlayer.color.equals("white")) ? "black flag" : "white flag");
		// reset check flag
		if (otherPlayer.color.equals("white")) this.blackUnderCheck = false;
		else this.whiteUnderCheck = false;
		return false;
	}

	/**
	 * Function to determine if the game has reached checkmate. Is to be called before the next player's turn.
	 * Utilizes getPath function to determine if any pieces can move to block this path.
	 */
	private void testCheckmate() {
		int count = 0;
		boolean looksLikeCheckmate = true;
		ArrayList<String> blockCheckSpots = new ArrayList<String>();
		Player player = !this.whiteMove ? this.white : this.black;
		// compile a list of spots (path) for each check from a piece (except king).
		for (String s : this.checks) {
			blockCheckSpots.addAll(getPath(s));
			// System.out.println(getPath(s));
		}
		// for each path, determine if at least 1 piece can move there and reset the check
		for (String spot : blockCheckSpots) {
			for (Piece p : player.pieces) {
				if (p.name.charAt(1) != 'K' && this.canMove(p.location, spot)) {
					// System.out.println(p.name+" can block "+p.location);
					looksLikeCheckmate = false;
					count ++;
				}
			}
			this.checkmate = looksLikeCheckmate;
		}
		// find out how many moves the king can make to escape the check
		Piece king = player.findPiece(player.getKingLocation());
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				if (i == 0 && j == 0) {
					continue;
				}
				String newLocation = Piece.encodeLocation(Piece.getRank(king.location)+i, Piece.getFile(king.location)+j);
				if (!blockCheckSpots.contains(newLocation) && this.canMove(king.location, newLocation) && !testUnderCheck(newLocation)) {
					// System.out.println("king can move to "+newLocation);
					looksLikeCheckmate = false;
					count ++;
				}
			}
		}
		this.checkmate = looksLikeCheckmate;

		// if only one move, be ready for a checkmate
		if (count == 1)
			this.onlyMove = true;
	}

	/**
	 * Function to return the full path in a step-by-step format.
	 * Example: a4 e8 -> a4 b5 c6 d7
	 * @param move - similar to the input the user provides (eg.: a4 e8)
	 * @return - an ArrayList of locations
	 */
	private ArrayList<String> getPath(String move) {
		String curr = move.split(" ")[0];
		String next = move.split(" ")[1];
		int rank = Piece.getRank(curr), file = Piece.getFile(curr);
		int newRank = Piece.getRank(next), newFile = Piece.getFile(next);
		ArrayList<String> blockCheckSpots = new ArrayList<String>();
		// diagonal
		if (Math.abs(rank-newRank) == Math.abs(file-newFile)) {
			int steps = Math.abs(rank-newRank);
			int step = rank < newRank ? 1 : -1;
			blockCheckSpots.add(curr);
			for (int i = 0; i < steps; i++) {
				blockCheckSpots.add(Piece.encodeLocation(rank+step, file+step));
				rank += step;
				file += step;
			}
		}
		// up/down
		if (file-newFile == 0 && rank-newRank != 0) {
			int steps = Math.abs(rank-newRank);
			int step = rank-newRank < 0 ? 1 : -1;
			blockCheckSpots.add(curr);
			for (int i = 0; i < steps; i++) {
				blockCheckSpots.add(Piece.encodeLocation(rank+step, file));
				rank += step;
			}
		}
		// side to side
		if (file-newFile != 0 && rank-newRank == 0) {
			int steps = Math.abs(file-newFile);
			int step = file-newFile < 0 ? 1 : -1;
			blockCheckSpots.add(curr);
			for (int i = rank; i < steps; i++) {
				blockCheckSpots.add(Piece.encodeLocation(rank, file+step));
				file += step;
			}
		}
		return blockCheckSpots;
	}

	
	public void undo() {
		if (this.lastMove.equals(""))
			return;
		String curr = this.lastMove.split(" ")[1].trim();
		String old = this.lastMove.split(" ")[0].trim();
		Piece p = this.white.findPiece(curr) == null ? this.black.findPiece(curr) : this.white.findPiece(curr);
		if (p != null) {
			p.undo();
		}
		if (this.lastCaptured != null && this.lastCaptured.location.equals(curr)) {
			if (this.lastCaptured.color.equals("black"))
				this.black.revive(this.lastCaptured);
			else if (this.lastCaptured.color.equals("white"))
				this.white.revive(this.lastCaptured);
		}
	}
	/**
	 * Move function checks the board to see if there are any obstacles in a path via
	 * a call to Chess.validatePath. If successful, invokes the canMove function in the Player
	 * Class
	 * It does not actually make the move
	 * @param curr - is the current (start) position
	 * @param next - is the final (end) position
	 * @return - true if path validation succeeds and call to Player.() succeeds 
	 */
	public boolean canMove(String curr, String next) {
		if (curr.equals("") || next.equals(""))
			return false;
		if(curr.charAt(0)<'a'||curr.charAt(0)>'h'||next.charAt(0)>'h'||next.charAt(0)<'a'){
			return false;
		}
		if(curr.charAt(1)<'1'||curr.charAt(1)>'8'||next.charAt(1)>'8'||next.charAt(1)<'1'){
			return false;
		}

		// check for obstacles
		// obstacles: any piece that prevents the piece from moving to the final destination (not a capture)
		// if it's a king or pawn, can only move 1 unit of distance (pawn can move 2 if first move
		String currPiece = this.board.getPiece(curr);

		//check if initial rank and file are no empty
		if (currPiece.equals(""))
			return false;

		// pawn is an exception since it captures differently
		else if (currPiece.charAt(1) == 'p')
		{
			if(Pawn(curr, next))
				return true;
			else return false;
		}

		//validate move to be up/down/diagonal
		else {
			if (testValidatePath(curr, next)) {
				// proceed to move validation at Piece level
				if (this.board.getPiece(curr).charAt(0) == 'w') return this.white.canMove(curr, next);
				else return this.black.canMove(curr, next);
			}
		}
		// failed path validation at game level
		return false;
	}


	/**
	 * Move function checks the board to see if there are any obstacles in a path via
	 * a call to Chess.validatePath. If successful, invokes the move function in the Player
	 * Class
	 * @param curr - is the current (start) position
	 * @param next - is the final (end) position
	 * @return - true if path validation succeeds and call to Player.() succeeds 
	 */
	public boolean move(String curr, String next) {
		if (curr.equals("") || next.equals(""))
			return false;
		if(curr.charAt(0)<'a'||curr.charAt(0)>'h'||next.charAt(0)>'h'||next.charAt(0)<'a'){
			return false;
		}
		if(curr.charAt(1)<'1'||curr.charAt(1)>'8'||next.charAt(1)>'8'||next.charAt(1)<'1'){
			return false;
		}

		// check for obstacles
		// obstacles: any piece that prevents the piece from moving to the final destination (not a capture)
		// if it's a king or pawn, can only move 1 unit of distance (pawn can move 2 if first move
		String currPiece = this.board.getPiece(curr);

		//check if initial rank and file are no empty
		if (currPiece.equals(""))
			return false;

		// pawn is an exception since it captures differently
		else if (currPiece.charAt(1) == 'p')
		{
			if(Pawn(curr, next))
				return true;
			else return false;
		}

		//validate move to be up/down/diagonal
		else {
			if (validatePath(curr, next)) {
				// proceed to move validation at Piece level
				if (this.whiteMove) return this.white.move(curr, next);
				else return this.black.move(curr, next);
			}
		}
		// failed path validation at game level
		return false;
	}
	
	/**
	 * Checks validity of pawn movements including enpasssant
	 * @param curr
	 * @param next
	 * @return
	 */
	public boolean Pawn(String curr, String next)
	{
		pawncaptured=false;
		//make sure enpassant move is played in the very next turn
		if((enpassant_color.equals("white")&&this.whiteMove)||(enpassant_color.equals("black")&&!this.whiteMove))
			enpassant =false;

		int rankChange = Math.abs(Piece.getRank(next)-Piece.getRank(curr));
		//pawn capture
		if(PawnCapture(curr,next))
		{
			pawncaptured=true;
			if (this.whiteMove) 
				{
					return this.white.move(curr, next);
				}
			else return this.black.move(curr, next);

		}
		//pawn validation
		else 
		{
			if (this.board.getPiece(next).equals("")) {
				// proceed to move validation at Piece level
				if (this.whiteMove) 
				{	
					boolean a = this.white.move(curr, next);
					if(rankChange ==2 && a)
					{
						enpassant = true;
						enpassant_color = "white";
						return true;
					}
					else 
						return a;
				}
				else 
				{
					boolean b = this.black.move(curr, next);
					if(rankChange ==2 && b)
					{
						enpassant = true;
						enpassant_color = "black";
						return true;
					}
					else 
						return b;
				}
			}
			return false;
		}

	}

	/**
	 * 
	 * @param curr - current location of the king
	 * @param next - final location of the king
	 * @param rlocation - location the rook is at
	 * @param color - color of the piece
	 * king has not moved, rook has not moved
	 * king must not come in a check position
	 * 	king must not come in a position where it can be attacked
	 * 	no castling in check
	 * 	no pieces between the king and the rook
	 * @return true is castling is successful else false
	 */
	public boolean castle(String curr, String next, String rlocation, String color)
	{
		//System.out.println("in castling");
		int rrank = Piece.getRank(rlocation);
		int rfile = Piece.getFile(rlocation);
		int krank = Piece.getRank(curr);
		int kfile = Piece.getFile(curr);
		incastling = false;
		//System.out.println("castled:" +this.castled);
		//checking if pieces are present between the king and the rook
		int steps = Math.abs(kfile-rfile);
		int step = kfile-rfile < 0 ? 1 : -1;
		for (int i = 0; i < steps-1; i++) {
			if (!this.board.getPiece(Piece.encodeLocation(krank, kfile+step)).equals(""))
				return false;
			// update file
			kfile += step;
		}

		//white king castling
		if(this.whiteMove)
		{
			//white king moving to the left
			if(next.equals("g1")&&this.white.move(curr, next))
			{
				//System.out.println("king moved");
				incastling = true;
				//rook moving
				if (this.white.move(rlocation, "f1" ))
					this.castled=true;
				return castled;
			}
			//white king moving to the right
			else if(next.equals("b1")&&this.white.move(curr, next))
			{
				incastling = true;
				//rook moving
				if(this.white.move(rlocation, "c1" ))
					this.castled = true;
				return castled;
			}
			else 
				return false;		
		}

		else
		{
			//black king moving to the left
			if(next.equals("g8")&&this.black.move(curr, next))
			{
				incastling = true;
				//rook moving
				if (this.black.move(rlocation, "f8" ))
					this.castled=true;
				return castled;
			}
			//black king moving to the right
			else if(next.equals("b8")&&this.black.move(curr, next))
			{
				incastling = true;
				//rook moving
				if(this.black.move(rlocation, "c8" ))
					this.castled = true;
				return castled;
			}
			else 
				return false;		
		}

	}


	public static void main(String[] args) {
		Chess game = new Chess();
		Scanner sc = new Scanner(System.in);
		String curr, next;
		while (true) {
			game.printBoard();

			// perform check detection
			if (game.checkmate) {
				System.out.println("Checkmate");
				System.out.println((!game.whiteMove) ? "White wins": "Black wins");
				break;
			}

			if (game.whiteUnderCheck || game.blackUnderCheck)
				System.out.println("Check");

			if (game.whiteMove)
				System.out.print("White move: ");
			else
				System.out.print("Black move: ");

			// get a valid move, then update the board
			String line = sc.nextLine();
			// System.out.println(line);
			// retry for input upon invalid input
			while (line.split(" ").length != 2) {
				// case of resign
				if (line.toLowerCase().equals("resign")) {
					System.out.println((!game.whiteMove) ? "White wins": "Black wins");
					game.finished = true;
					break;
				}
				// case of draw
				if (line.toLowerCase().trim().equals("draw")) {
					System.out.println("Game ends in draw");
					game.finished = true;
					break;
				}
				// case of 3 inputs
				if (line.split(" ").length == 3) {
					if (line.split(" ")[2].toLowerCase().trim().equals("draw?"))
						game.drawOffer = true;
					// check if third input is promotion.
					System.out.println("3 args, promotion?");
					if (isPiece(line.split(" ")[2].trim())) {
						System.out.println("promotion");
						curr = line.split(" ")[0].toLowerCase().trim();
						next = line.split(" ")[1].toLowerCase().trim();
						String piece = line.split(" ")[2].trim();
						// invoke promotion, handles updateBoard, check detection, next player turn
						game.promote(curr, next, piece);
						game.lastMove = curr+" "+next;
					}
					break;
				}
				else {
					System.out.println("Illegal move, try again");
					line = sc.nextLine();
				}
			}
			if (game.finished)
				break;
			curr = line.split(" ")[0].toLowerCase().trim();
			next = line.split(" ")[1].toLowerCase().trim();

			//castling

			//white king castling to the left
			if(game.board.getPiece(line.split(" ")[0]).equals("wK")&& line.split(" ")[0].equals("e1")&& line.split(" ")[1].equals("g1")&&!game.whiteUnderCheck)
			{
				if(game.castle(line.split(" ")[0],line.split(" ")[1],"h1", "white" ))
				{
					// set up board for the next player turn
					game.lastMove = curr+" "+next;
					game.board.updateBoard(game.white, game.black);
					game.underCheck();
					// if check flag has not reset, undo move
					if ((game.whiteMove && game.whiteUnderCheck) || (!game.whiteMove && game.blackUnderCheck)) {
						game.undo();
						//System.out.println("UNDO");
						game.board.updateBoard(game.black, game.white);
						if (game.onlyMove) {
							game.checkmate = true;
							continue;
						}
						System.out.println("Illegal move, try again");
						continue;
					}
					game.whiteMove = !game.whiteMove;

				}
			}
			//white king castling to the right
			else if(game.board.getPiece(line.split(" ")[0]).equals("wK")&& line.split(" ")[0].equals("e1")&& line.split(" ")[1].equals("b1")&&!game.whiteUnderCheck)
			{
				if(game.castle(line.split(" ")[0],line.split(" ")[1],"a1", "white" ))
				{
					// set up board for the next player turn
					game.lastMove = curr+" "+next;
					game.board.updateBoard(game.white, game.black);
					game.underCheck();
					// if check flag has not reset, undo move
					if ((game.whiteMove && game.whiteUnderCheck) || (!game.whiteMove && game.blackUnderCheck)) {
						game.undo();
						//System.out.println("UNDO");
						game.board.updateBoard(game.black, game.white);
						if (game.onlyMove) {
							game.checkmate = true;
							continue;
						}
						System.out.println("Illegal move, try again");
						continue;
					}
					game.whiteMove = !game.whiteMove;
				}
			}
			//black king castling to the left
			else if(game.board.getPiece(line.split(" ")[0]).equals("bK")&& line.split(" ")[0].equals("e8")&& line.split(" ")[1].equals("g8")&&!game.blackUnderCheck)
			{
				if(game.castle(line.split(" ")[0],line.split(" ")[1],"h8", "black" ))
				{
					// set up board for the next player turn
					game.lastMove = curr+" "+next;
					game.board.updateBoard(game.white, game.black);
					game.underCheck();
					// if check flag has not reset, undo move
					if ((game.whiteMove && game.whiteUnderCheck) || (!game.whiteMove && game.blackUnderCheck)) {
						game.undo();
						//System.out.println("UNDO");
						game.board.updateBoard(game.black, game.white);
						if (game.onlyMove) {
							game.checkmate = true;
							continue;
						}
						System.out.println("Illegal move, try again");
						continue;
					}
					game.whiteMove = !game.whiteMove;
				}
			}
			//black king castling to the right
			else if(game.board.getPiece(line.split(" ")[0]).equals("bK")&& line.split(" ")[0].equals("e8")&& line.split(" ")[1].equals("b8")&&!game.blackUnderCheck)
			{
				if(game.castle(line.split(" ")[0],line.split(" ")[1],"a8", "black" ))
				{
					// set up board for the next player turn
					game.lastMove = curr+" "+next;
					game.board.updateBoard(game.white, game.black);
					game.underCheck();
					// if check flag has not reset, undo move
					if ((game.whiteMove && game.whiteUnderCheck) || (!game.whiteMove && game.blackUnderCheck)) {
						game.undo();
						//System.out.println("UNDO");
						game.board.updateBoard(game.black, game.white);
						if (game.onlyMove) {
							game.checkmate = true;
							continue;
						}
						System.out.println("Illegal move, try again");
						continue;
					}
					game.whiteMove = !game.whiteMove;
				}
			}

			if (!game.drawOffer && !game.promotion&&!game.castled) {
				curr = line.split(" ")[0].toLowerCase().trim();
				next = line.split(" ")[1].toLowerCase().trim();
				if (game.move(curr, next)) {
					// set up board for the next player turn
					game.lastMove = curr+" "+next;
					game.board.updateBoard(game.white, game.black);
					game.underCheck();
					// if check flag has not reset, undo move
					if ((game.whiteMove && game.whiteUnderCheck) || (!game.whiteMove && game.blackUnderCheck)) {
						game.undo();
						//System.out.println("UNDO");
						game.board.updateBoard(game.black, game.white);
						if (game.onlyMove) {
							game.checkmate = true;
							continue;
						}
						System.out.println("Illegal move, try again");
						continue;
					}
					game.whiteMove = !game.whiteMove;
				}
				else
				System.out.println("Illegal move, try again");
			}
			System.out.println();
			// reset some game state
			game.castled = false;
			game.promotion = false;
			game.onlyMove = false;
		}
		sc.close();
	}
}
