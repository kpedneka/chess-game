package player;

import java.util.ArrayList;

import piece.Bishop;
import piece.King;
import piece.Knight;
import piece.Pawn;
import piece.Piece;
import piece.Queen;
import piece.Rook;
/**
 * Player class initializes an array of pieces for each player (black and white)
 * @author Kunal Pednekar(ksp101)
 *
 */
public class Player {
	/**
	 * {@link color} - color (side) of player
	 */
	public String color;
	/**
	 * {@link pieces} - set of pieces that belong to each color
	 */
	public ArrayList<Piece> pieces = new ArrayList<Piece>();
	
	/**
	 * Constructor for a Player. Initializes all 16 pieces for the player
	 * @param color - can have values "black" or "white".
	 */
	public Player(String color) {
		this.color = color;
		int count = 0;
		String rank, file;
		// 8 pawns
		for(int i = 0; i < 8; i++) {
			rank = this.color.equals("black") ? "7" : "2";
			file = Character.toString((char) (i+'a'));
			this.pieces.add(new Pawn (this.color, file+rank));
			count++;
		}
		rank = this.color.equals("black") ? "8" : "1";
		for(int i = 0; i < 4; i++) {
			String file1 = Character.toString((char) (i+'a'));
			String file2 = Character.toString((char)(7-i+'a'));
			
			if (i == 0) {
				// 2 rooks
				this.pieces.add(new Rook (this.color, file1+rank));
				this.pieces.add(new Rook (this.color, file2+rank));
				count += 2;	
			} else if (i == 1) {
				// 2 knights
				this.pieces.add(new Knight (this.color, file1+rank));
				this.pieces.add(new Knight (this.color, file2+rank));
				count += 2;
			} else if (i == 2){
				// 2 bishops
				this.pieces.add(new Bishop (this.color, file1+rank));
				this.pieces.add(new Bishop (this.color, file2+rank));
				count += 2;
			} else {
				// king & queen
				this.pieces.add(new Queen (this.color, file1+rank));
				this.pieces.add(new King (this.color, file2+rank));
			}	
		}
	}
	
	/**
	 * Function invoked by implicit and explicit pawn promotion functions
	 * @param p - Pawn piece to be promoted
	 * @param promoteTo - rank/class of the desired promotion
	 */
	private void switchPieces(Pawn p, String promoteTo) {
		Piece newP;
		switch (promoteTo) {
			case "N":
				newP = new Knight(p.color, p.location);
			case "B":
				newP = new Bishop(p.color, p.location);
			case "R":
				newP = new Rook(p.color, p.location);
			default:
				newP = new Queen(p.color, p.location);
		}
		// System.out.println("killing "+p.name+" and adding "+newP.name+" to the list at "+newP.location);
		this.kill(p.location);
		this.pieces.add(newP);
	}
	
	/**
	 * Explicit pawn promotion function
	 * @param location - location of current piece
	 * @param promoteTo - rank/class of the desired promotion
	 * @return - true if piece is found and rank is correct, false otherwise
	 */
	public boolean promote(String location, String promoteTo) {
		Piece p = findPiece(location);
		if (p == null) {
			//System.out.println("Ilegal move, try again");
			return false;
		}
		//System.out.println(this.color+" "+p.location+" "+Piece.getRank(p.location));
		if (p instanceof Pawn) {
			if ((this.color.equals("black") && Piece.getRank(p.location) == 1) || (this.color.equals("white") && Piece.getRank(p.location) == 8)) {
				switchPieces((Pawn) p, promoteTo);
				return true;
			}	
		}
		return false;
	}
	
	/**
	 * Function that is invoked from the game to indicate a piece is captured
	 * @param location - the location of the piece to be eliminated
	 * @return - true if a piece is indeed at the location, false otherwise
	 */
	public boolean kill(String location) {
		Piece p = findPiece(location);
		if (p == null)
			return false;
		return this.pieces.remove(p);
	}
	
	/**
	 * Revive function to revive a piece that was captured in the previous move.
	 * Invoked by undo function only.
	 * @param p - Piece that was last captured
	 */
	public void revive(Piece p) {
		//System.out.println("revived "+p.name);
		this.pieces.add(p);
	}
	
	/**
	 * Finds a piece at a location on a board.
	 * @param location - String that represents a location on a chess board (eg.: e4)
	 * @return - Piece object if present at location, null otherwise.
	 */
	public Piece findPiece(String location) {
		for (Piece p : this.pieces) {
			if (p.location.equals(location))
				return p;
		}
		return null;
	}

	/**
	 * Function to locate position of king
	 * @return - String location of the king on the board
	 */
	public String getKingLocation() {
		for (Piece p : this.pieces) {
			if (p.name.charAt(1) == 'K')
				return p.location;
		}
		return "Game should be over";
	}
	
	/**
	 * canMove function invokes the canMove function for each piece. Through polymorphism,
	 * we are able to allow each type of Piece to validate and update it's position. This
	 * function does not actually change the position of a piece.
	 * @param location - String that represents a location on a chess board (eg.: e4)
	 * @param newLocation - String that represents the new location on a chess board (eg.: e5)
	 * @return - true if move is successful, false otherwise
	 */
	public boolean canMove(String location, String newLocation) {
		Piece p = findPiece(location);
		Piece p2 = findPiece(newLocation);
		if (p == null) {
			// System.out.println("No piece found at location "+location+". Try again");
			return false;	
		}
		if (p2 != null) {
			// System.out.println("Cannot move to location. Illegal move");
			return false;
		}
		return p.canMove(newLocation);
	}
	
	/**
	 * Move function invokes the move function for each piece. Through polymorphism,
	 * we are able to allow each type of Piece to validate and update it's position.
	 * @param location - String that represents a location on a chess board (eg.: e4)
	 * @param newLocation - String that represents the new location on a chess board (eg.: e5)
	 * @return - true if move is successful, false otherwise
	 */
	public boolean move(String location, String newLocation) {
		Piece p = findPiece(location);
		Piece p2 = findPiece(newLocation);
		if (p == null) {
			//System.out.println("No piece here. Illegal move, try again");
			return false;	
		}
		if (p2 != null) {
			//System.out.println("A peice there. Illegal move, try again"+p2.name);
			return false;
		}
		return p.move(newLocation);
	}
}
