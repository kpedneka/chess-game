package board;

import piece.Piece;
import player.Player;

/**
 * 
 * @author Kunal Pednekar(ksp101)
 *
 */
public class Board {
	String[][] board;
	/**
	 * This method initializes a new game board
	 * @return String[][] which represents the board
	 */
	public Board() {
		board = new String[8][8];
		final String blank = "  ";
		final String fill = "##";
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board[i].length; j++) {
				if (i % 2 == 0) {
					if (j % 2 == 0)
						board[i][j] = blank;
					else
						board[i][j] = fill;
				} else {
					if (j % 2 == 0)
						board[i][j] = fill;
					else
						board[i][j] = blank;
				}
			}
		}
	}
	
	/**
	 * Controller function that updates the state of the board
	 * @param black - Object of class Player. The black player
	 * @param white - Object of class Player. The white player
	 */
	public void updateBoard(Player black, Player white) {
		// make a shallow copy of the board
		Board game = new Board();
		// go through pieces and overwrite the board tiles with the piece
		for (Piece p : black.pieces) {
			int row = 8-Piece.getRank(p.location);
			int column = Piece.getFile(p.location);
			game.board[row][column] = p.name;
		}
		for (Piece p : white.pieces) {
			int row = 8-Piece.getRank(p.location);
			int column = Piece.getFile(p.location);
			game.board[row][column] = p.name;
		}
		this.board = game.board;
	}
	
	/**
	 * This method prints the board
	 */
	public void printBoard() {
		// print the board with the pieces occupying the tiles
		System.out.println();
		for(int i = 0; i <= this.board.length; i++) {
			for(int j = 0; j <= this.board[0].length; j++) {
				// print bottom right empty space in board
				if (i == this.board.length && j == this.board.length)
					System.out.println("  ");
				// print files
				else if (i == this.board.length && j != this.board.length)
					System.out.print(" "+(char)(j + 'a')+ " "); 
				// print ranks
				else if(i != this.board.length && j == this.board.length)
					System.out.println(8-i);
				// print the tile of the board
				else
					System.out.print(this.board[i][j]+" ");
			}
		}	
	}
	
	/**
	 * Gets the piece name at the location on a board
	 * @param location - location on the board to get piece from
	 * @return - empty string upon empty tile, otherwise returns Piece name at the location.
	 */
	public String getPiece(String location) {
		int row = 8-Piece.getRank(location);
		int column = Piece.getFile(location);
		if (this.board[row][column] == "  " || this.board[row][column] == "##") {
			// the tile is empty
			return "";
		}
		// this is a piece name
		//System.out.println(this.board[row][column]);
		return this.board[row][column];
	}
}
